FROM python:3.8

RUN curl -LO https://github.com/mozilla/geckodriver/releases/download/v0.33.0/geckodriver-v0.33.0-linux64.tar.gz
RUN tar xvfz geckodriver-v0.33.0-linux64.tar.gz && mv geckodriver /usr/local/bin/geckodriver

RUN apt-get update 

# RUN apt-get install -y firefox
RUN apt-get install -y firefox-esr

RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
